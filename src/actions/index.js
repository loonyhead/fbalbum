import * as types from '../Constants';

export function init(){
    window.fbAsyncInit = function() {
        window.FB.init({
            appId            : '342652216369185',
            autoLogAppEvents : true,
            xfbml            : true,
            version          : 'v3.2',
        });
    };
    return {
        type: types.FB_INIT,
    }
};

export function login(){
    return dispatch => {
        window.FB.login(
            res => {
                if(res.status==="connected"){
                        dispatch(getProfileDetails());
                        dispatch({
                            type: types.FB_LOGIN_SUCCESS,
                            payload: res.authResponse, 
                            notify: {
                                status: true,
                                msg: "FB Login Successful",
                            }
                        });
                }else{
                        dispatch({
                            type: types.FB_LOGIN_FAILURE,
                            notify: {
                                status: true,
                                msg: "FB Login Unsuccessful",
                            }
                        });
                }
            },
            {scope: 'email, public_profile'}
        );
    }
};

export function logout(){
    return dispatch => {
        window.FB.logout();
        dispatch({
            type: types.FB_LOGOUT, 
            notify: {
                status: true,
                msg: "Logged out successfully",
            }
        });
    }
}

function getProfileDetails(){
    return dispatch => {
        window.FB.api('/me?fields=name,email,picture.type(large),albums{name,count,cover_photo{picture}}', (res)=>{
            if(res && res.error) { //if error
                dispatch(getProfileDetailsFailed());
            } else if(!res || !res.hasOwnProperty('albums')) {
                if(typeof failCallback === 'function'){
                    dispatch(getProfileDetailsFailed());
                }   
            }
            let payload = {
                email: res.email,
                name: res.name,
            };
            if(res.picture && res.picture.data){
                payload["profilePic"]= res.picture.data.url;
            }
            payload["albums"] = res.albums.data.map(el=>{
                return {
                    id: el.id,
                    name: el.name,
                    count: el.count,
                    photo: el.cover_photo.picture,
                }
            });
            dispatch({
                type: types.PROFILE_DETAILS_SUCCESS,
                payload, 
            });
        });
    }
}

function getProfileDetailsFailed(){
    return {
        type: types.PROFILE_DETAILS_FAILURE,
        notify: {
            status: true,
            msg: "Some error occured while fetching your details",
        }
    }
}

export function getPhotos(id){
    return dispatch => {
        dispatch({
            type: types.GET_PHOTOS_LOADING,
        });
        window.FB.api(id+'/?fields=photos{images},name', (res)=>{
            if(res && res.error) { //if error
                dispatch(getPhotosFailed());
            } else if(!res || !res.hasOwnProperty('albums')) {
                if(typeof failCallback === 'function'){
                    dispatch(getPhotosFailed());
                }   
            }
            let payload = {
                openedAlbumId : id,
                openedAlbumName: res.name,
            }

            payload["openedPhotos"] = res.photos.data.map(el=>{
                return el.images[0].source
            });
            
            dispatch({
                type: types.GET_PHOTOS_SUCCESS,
                payload, 
            });

        });
    }
}

function getPhotosFailed(){
    return {
        type: types.GET_PHOTOS_FAILURE,
        notify: {
            status: true,
            msg: "Some error occured while fetching photos to the album",
        }
    }
}

export function openInLightBox(pos){
    return {
        type: types.OPEN_LIGHT_BOX,
        payload: pos,
    }
}

export function closeLightBox(){
    return {
        type: types.CLOSE_LIGHT_BOX,
    }
}

export function goBackToHome(){
    return {
        type: types.GO_BACK_HOME,
    }
}