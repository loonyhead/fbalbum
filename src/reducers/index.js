import * as types from '../Constants';

const initialState = {
    isAuthenticated: false,
    loadingDetails: false,
    loadingPhotos: false,
    authObject: null,
    name: null,
    email: null,
    albums: null,
    openedAlbumId: null,
    openedAlbumName: null,
    openedPhotos: null,
    isLightBoxOpen: false,
    lightBoxOpenPosition: null,
};

const fbReducer = (state=initialState, action) => {
    let newState = {};

    switch(action.type){

        case types.FB_INIT : {
            return state;
        }

        case types.FB_LOGIN_SUCCESS : {
            newState["isAuthenticated"] = true;
            newState["authObject"] = action.payload;  
            newState["loadingDetails"] = true;          
            return Object.assign({}, state, newState);
        }

        case types.FB_LOGOUT : {
            return Object.assign({}, state, initialState);
        }

        case types.FB_LOGIN_FAILURE : {
            newState["isAuthenticated"] = false;       
            return Object.assign({}, state, newState);
        }

        case types.PROFILE_DETAILS_SUCCESS : {
            const { name, profilePic, email, albums } = action.payload;
            newState = {
                name,
                profilePic,
                email,
                albums,
            };        
            newState["loadingDetails"] = false; 
            return Object.assign({}, state, newState);
        }

        case types.GET_PHOTOS_SUCCESS : {
            const {openedAlbumId, openedAlbumName, openedPhotos} = action.payload;
            newState = {
                openedAlbumId,
                openedAlbumName,
                openedPhotos,
                loadingPhotos: false,
            };
            return Object.assign({}, state, newState);
        }

        case types.GET_PHOTOS_LOADING : {
            newState["loadingPhotos"] = true;
            newState["isLightBoxOpen"] = false;
            newState["lightBoxOpenPosition"] = null;
            return Object.assign({}, state, newState);
        }

        case types.GO_BACK_HOME : {
            newState["isLightBoxOpen"] = false;
            newState["lightBoxOpenPosition"] = null;
            newState["openedAlbumId"] = null;
            newState["openedAlbumName"] = null;
            newState["openedPhotos"] = null;
            return Object.assign({}, state, newState);
        }

        case types.OPEN_LIGHT_BOX : {
            newState["isLightBoxOpen"] = true;
            newState["lightBoxOpenPosition"] = action.payload;
            return Object.assign({}, state, newState);
        }

        case types.CLOSE_LIGHT_BOX : {
            newState["isLightBoxOpen"] = false;
            newState["lightBoxOpenPosition"] = null;
            return Object.assign({}, state, newState);
        }

        default:
            return state;
    }
}

export default fbReducer;
