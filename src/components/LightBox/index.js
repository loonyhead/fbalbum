import React from 'react';
import cx from 'classnames';
import style from './style.module.scss';
import { ReactComponent as Left } from './left.svg';
import { ReactComponent as Right } from './right.svg';
import { ReactComponent as Close } from './close.svg';

class LightBox extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            pos: props.pos,
            photos: props.photos,
            isOpen: false,
        };
        this.close=this.close.bind(this);
        this.next=this.next.bind(this);
        this.prev=this.prev.bind(this);
    }

    componentDidMount(){
        document.body.style.overflow = "hidden";
        setTimeout(
            function() {
                this.setState({isOpen: true});
            }
            .bind(this), 0
        );
    }

    close(){
        document.body.style.overflow = "auto";
        this.setState({isOpen: false});
        setTimeout(
            function() {
                this.props.close();
            }
            .bind(this), 200
        );
    }

    next(){
        const {pos, photos} = this.state;
        let newPos = pos+1 > photos.length-1 ? 0 : pos+1;  
        this.setState({
            pos: newPos,
        }); 
    }

    prev(){
        const {pos, photos} = this.state;
        let newPos = pos-1 < 0 ? photos.length-1 : pos-1;  
        this.setState({
            pos: newPos,
        }) 
    }

    componentWillUnmount(){
        document.body.style.overflow = "auto";
    }

    render() {
        const {pos, photos, isOpen} = this.state;
        const total = photos.length;
        const cStyle = isOpen ? cx(style.lightBox, style.open) : style.lightBox;
        return (
            <div className={cStyle}>
                <div className={style.header}>
                    <div className={style.text}>
                        {pos+1} of {total} photo(s)
                    </div>
                    <div className={style.close} onClick={this.close}>
                        <Close />
                    </div>
                </div>
                <div className={style.canvas}>
                    <img src={photos[pos]} onClick={this.next} />
                </div>
                <div className={style.footer}>
                    <div className={style.prev} onClick={this.prev}>
                        <Left />
                    </div>
                    <div className={style.next} onClick={this.next}>
                        <Right />
                    </div>
                </div>
            </div>
        );
    }
}

export default LightBox;
