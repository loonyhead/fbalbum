import React, { Component } from 'react';
import style from './style.module.scss';

class AlbumCard extends Component {
    constructor(){
        super();
        this.state = {
            
        };
        this.onClick=this.onClick.bind(this);
    }

    onClick(){
        const {id, name} = this.props;
        this.props.getPhotos(id);
    }

    render() {
        const { name, count, photo } = this.props;
        return (
            <div className={style.albumCard} onClick={this.onClick}>
                <div className={style.albumPic}>
                    <img src={photo} />
                    <div className={style.overlay}>
                        <div>{count} Photo(s)</div>
                    </div>
                </div>
                <div className={style.albumName}>
                    {name}
                </div>
            </div>
        );
    }
}

export default AlbumCard;