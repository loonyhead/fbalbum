import React from 'react';
import style from './style.module.scss';

class Layout extends React.Component {
    
    render() {
        return (
            <div className={style.layout}>
                <header className={style.header}>
                    <div className={style.logo}>
                        FB PHOTO GALLERY
                    </div>
                </header>
                <main className={style.content}>
                    {this.props.children}
                </main>
            </div>
        );
    }
}

export default Layout;
