import React, { Component } from 'react';
import style from './style.module.scss';

class PhotoCard extends Component {
    constructor(){
        super();
        this.onClick=this.onClick.bind(this);
    }

    onClick(){
        const {pos} = this.props;
        this.props.openInLightViewer(pos);
    }

    render() {
        const { src } = this.props;
        return (
            <div className={style.photoCard} onClick={this.onClick}>
                <img src={src} />
            </div>
        );
    }
}

export default PhotoCard;