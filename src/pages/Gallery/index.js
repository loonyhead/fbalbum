import React from 'react';
import { connect } from "react-redux";
import {withRouter} from 'react-router-dom';
import * as fbActions from '../../actions';
import { PhotoCard } from '../../components';
import style from './style.module.scss';
import { ReactComponent as Left } from './left.svg';

class Gallery extends React.Component {
    constructor(){
        super();
        this.state = {
            
        };
        this.openInLightViewer=this.openInLightViewer.bind(this);
        this.onBackButtonEvent=this.onBackButtonEvent.bind(this);
    }

    componentWillMount(){
        const { id } = this.props.match.params; 
        this.props.dispatch(fbActions.getPhotos(id));
    }

    componentDidMount(){
        window.onpopstate = this.onBackButtonEvent;
    }

    onBackButtonEvent(){
        this.props.dispatch(fbActions.goBackToHome());
    }

    openInLightViewer(pos){
        this.props.dispatch(fbActions.openInLightBox(pos));
    }

    componentDidUnmount() {
        window.onpopstate = () => {}
    }

    render() {
        const {openedPhotos, openedAlbumName, loadingPhotos} = this.props;
        return (
            <div className={style.gallery}>
                <div className={style.goBack}>
                    <div>{openedAlbumName}</div>
                </div>
                <div className={style.photoGrid}>
                    {!loadingPhotos &&
                        openedPhotos.map((el, i)=>{
                            return <PhotoCard src={el} pos={i} openInLightViewer={this.openInLightViewer} />
                        })
                    }
                    {loadingPhotos &&
                        <div className={style.loading}>LOADING...</div>
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        openedAlbumId: state.openedAlbumId,
        openedAlbumName: state.openedAlbumName,
        openedPhotos: state.openedPhotos,
        loadingPhotos: state.loadingPhotos,
    };
};

export default withRouter(connect(mapStateToProps)(Gallery));
