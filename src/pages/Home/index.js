import React from 'react';
import { connect } from "react-redux";
import {withRouter} from 'react-router-dom';
import * as fbActions from '../../actions';
import { AlbumCard } from '../../components';
import style from './style.module.scss';

class Home extends React.Component {
    constructor(){
        super();
        this.state = {
            
        };
        this.login=this.login.bind(this);
        this.getPhotos=this.getPhotos.bind(this);
        this.logout=this.logout.bind(this);
    }

    componentWillMount(){
        this.props.dispatch(fbActions.init());
    }

    login(){
        this.props.dispatch(fbActions.login());
    }

    logout(){
        this.props.dispatch(fbActions.logout());
    }

    getPhotos(id){
        this.props.history.push('/album/'+id);
        this.props.dispatch(fbActions.getPhotos(id));
    }

    render() {
        const {isAuthenticated, loadingDetails, name, profilePic, email, albums} = this.props;
        return (
            <div className={style.home}>
                    {!isAuthenticated &&
                        <div className={style.connect}>
                            <h2 className={style.text}>
                                Start browsing your FB gallery
                            </h2>
                            <div className={style.fbBtn} onClick={this.login}>
                                Connect with FB
                            </div>
                        </div>
                    }
                    {isAuthenticated && !loadingDetails &&
                        <div className={style.info}>
                            <div className={style.profilePic}>
                                <img src={profilePic} />
                            </div>
                            <div className={style.bio}>
                                <div className={style.name}>
                                    {name}
                                </div>
                                <div className={style.email}>
                                    {email}
                                </div>
                                <div className={style.logout} onClick={this.logout}>
                                    Logout
                                </div>
                            </div>
                        </div>
                    }
                    {isAuthenticated && !loadingDetails &&
                        <div className={style.gallery}>
                            {
                                albums.map(el=>{
                                    return(
                                        <AlbumCard 
                                            key={el.id}
                                            id={el.id}
                                            name={el.name}
                                            photo={el.photo}
                                            count={el.count}
                                            getPhotos={this.getPhotos}
                                        />
                                    )
                                })
                            }
                        </div>
                    }
                    {isAuthenticated && loadingDetails &&
                        <div className={style.loading}>LOADING...</div>
                    }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.isAuthenticated,
        loadingDetails: state.loadingDetails,
        name: state.name,
        profilePic: state.profilePic,
        email: state.email,
        albums: state.albums,
    };
};

export default withRouter(connect(mapStateToProps)(Home));
