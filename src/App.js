import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { connect } from "react-redux";
import { Layout, LightBox } from './components';
import { Home, Gallery } from './pages';
import * as fbActions from './actions';
class App extends Component {

  constructor(){
      super();
      this.closeLightViewer=this.closeLightViewer.bind(this);
  }

  closeLightViewer(){
      this.props.dispatch(fbActions.closeLightBox());
  }

  render() {
    const {isLightBoxOpen, lightBoxOpenPosition, openedPhotos} = this.props;
    return (
        <Layout>
          <Router>
            <Switch>
              <Route exact path="/" render={()=><Home />} />  
              <Route exact path="/album/:id" render={id=><Gallery {...id} />} /> 
              <Redirect to="/" />
            </Switch>
          </Router>
          {isLightBoxOpen &&
            <LightBox pos={lightBoxOpenPosition} photos={openedPhotos} close={this.closeLightViewer} />
          }
        </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLightBoxOpen: state.isLightBoxOpen,
    lightBoxOpenPosition: state.lightBoxOpenPosition,
    openedPhotos: state.openedPhotos,
  };
};

export default connect(mapStateToProps)(App);