import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunk from 'redux-thunk';
import fbReducer from './reducers';

const middleware = [
  thunk
];

const enhancers = compose(
  applyMiddleware(...middleware),
  window.devToolsExtension ? window.devToolsExtension() : f => f
);

/**
* CREATE STORE
*/
const store = createStore(
  fbReducer,
  enhancers
);

export default store;
